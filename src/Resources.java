import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.Scanner;

public class Resources {

    private HashSet<String> resources;
    private String fileName;


    public Resources(String fileName) {
        this.fileName = fileName;
        this.resources = new HashSet<String>();
        ArrayList<String []> fileData;

        while(true) {
            try {
                fileData = getFileData(this.fileName);
                parseFileData(fileData);
                break;
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    public Resources() {
        this.fileName = "resourceObjects.txt";
        this.resources = new HashSet<String>();
    }

    public ArrayList<String []> getFileData(String fileName)
            throws IOException {
        ArrayList<String []> fileData = new ArrayList<String []>();
        Scanner scanner = new Scanner(new File(fileName));
        while(scanner.hasNextLine()) {
            String [] splitString = scanner.nextLine().toUpperCase().split("\\s+");
            int splitStringLength = splitString.length;
            if (splitStringLength > 0) {
                fileData.add(splitString);
            }
        }
        scanner.close();
        return fileData;
    } // End of getDataFromFile()

    public void parseFileData(ArrayList<String []> fileData)
            throws IOException {
        for(String[] x : fileData) {
            for(String y : x) {
                if (!this.resources.add(new String(y.toUpperCase()))) {
                    String message = "Duplicate Object: " + y;
                    throw new IOException(message);
                }
            }
        }
    }

    public HashSet<String> getResources() {
        return this.resources;
    }

    public void retry() {
        System.out.println("Fix the file then, press any key to continue.");
        Scanner s = new Scanner(System.in);
        String string = s.next();
        System.out.println("Trying again");
    }

}

