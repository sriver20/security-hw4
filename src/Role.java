import java.util.HashSet;

public class Role {

    private String name, parent;
    private HashSet<String> children, ancestors;


    public Role(String name) {
        this.name = name;
        this.children = new HashSet<String>();
        this.ancestors = new HashSet<String>();
    }

    public boolean isChild(String child) {
        return this.children.contains(child);
    }

    public boolean isAncestor(String ancestor) {
        return this.ancestors.contains(ancestor);
    }

    public boolean isParent(String parent) {
        if (this.parent != null)
            return this.parent.equalsIgnoreCase(parent);
        return false;
    }


    public boolean hasChildren() {
        if( this.children.size() == 0) { return false; }
        return true;
    }

    public boolean addChild(String child) {
        return this.children.add(child);
    }

    public boolean addAncestors(String ancestor) {
        return this.ancestors.add(ancestor);
    }

    public boolean setParent(String parent) {
        if (this.parent == null) { this.parent = parent; return true; }
        return false;
    }

    public String getParent() {
        return this.parent;
    }

    public String getName() {
        return this.name;
    }

    public String [] getChildren() {
        return this.children.toArray(new String[this.children.size()]);
    }

    public String [] getAncestors() {
        return this.ancestors.toArray(new String[this.ancestors.size()]);
    }

    @Override
    public String toString() {
        String message =  "Name: " + this.name + " Parent: "+ this.parent
                         + "\nChildren: ";

        for(String child : this.children) {
            message = message + child + " ";
        }

        message = message + "\nAncestors: ";

        for(String ancestor : this.ancestors) {
            message = message + ancestor + " ";
        }
        return message + "\n";
    }
}
