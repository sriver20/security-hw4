import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Scanner;
import java.util.HashSet;

public class UserMatrix {

    String fileName;
    SSD ssd;
    HashMap<String, HashSet<String>> matrix;
    HashSet<String> roles;



    public UserMatrix(String fileName, SSD ssd, HashSet<String> roles) {
        this.fileName = fileName;
        this.ssd = ssd;
        this.matrix = new HashMap<String, HashSet<String>>();
        this.roles = roles;

        while(true) {
            try {
                ArrayList<String[]> fileData = new ArrayList<String[]>();
                fileData = getFileData(this.fileName);
                parseFileData(fileData);
                break;
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            retry();
        }
        System.out.println(this);
    }

    public UserMatrix() {
        this.fileName = "usersRoles.txt";
        this.matrix = new HashMap<String, HashSet<String>>();
        this.ssd = new SSD();
    }

    public boolean isUser(String user) {
        return this.matrix.containsKey(user);
    }

    public HashSet<String> getRoles(String user) {
        return this.matrix.get(user);
    }

    public ArrayList<String []> getFileData(String FileName)
            throws IOException {
        ArrayList<String []> fileData = new ArrayList<String []>();
        Scanner scanner = new Scanner(new File(fileName));
        int lineCounter = 0;
        while(scanner.hasNextLine()) {
            lineCounter ++;
            String [] splitString = scanner.nextLine().toUpperCase().split("\\s+");
            int splitStringLength = splitString.length;
            if (splitStringLength < 1 ) {
                continue;
            }
            String [] tempArray = new String [splitStringLength + 1];
            int count = 0;
            for(String word : splitString) {
                tempArray[count] = new String(splitString[count]);
                count ++;
            }
            tempArray[splitStringLength] = Integer.toString(lineCounter);
            // for (String x : tempArray)
            //     System.out.print(" " + x);
            // System.out.println();
            fileData.add(tempArray);
        }
        scanner.close();
        return fileData;
    }

    public void parseFileData(ArrayList<String []> fileData)
            throws IOException {
        for(String[] line : fileData) {
            HashSet<String> roleSet = new HashSet<String>();
            String user = line[0];
            int indexOfLineNumber = line.length - 1;
            String lineNumber = line[indexOfLineNumber];
            if(this.matrix.containsKey(user)) {
                String message = "Duplicate user found on Line: " + lineNumber
                                 + " File Name: " + this.fileName;
                throw new IOException(message);
            }
            for(int i = 1; i < indexOfLineNumber; i++) {
                if(!this.roles.contains(line[i])) {
                    String message = "File Name: " + this.fileName + " Line: "
                                     + lineNumber + " Invalid Role: " + line[i];
                    throw new IOException(message);
                }
                roleSet.add(line[i]);
            }
            String[] roleArray = roleSet.toArray(new String[roleSet.size()]);
            if(!ssd.doRolesSatisfySSD(roleArray)) {
                String message = "Line: " + lineNumber
                                 + " breaks an SSD Constraint.";
                throw new IOException(message);
            }
            this.matrix.put(user,roleSet);
        }
    } //end of parseFileData()

    public void retry() {
        System.out.println("Fix the file then, press any key to continue.");
        Scanner s = new Scanner(System.in);
        String string = s.next();
        System.out.println("Trying again");
    }

    @Override
    public String toString() {
        String message = "User and Roles\n";
        for(String role : this.roles) {
            message = message + "    " + role;
        }
        message = message + "\n";
        int mapSize = matrix.size();
        for(String x : matrix.keySet()) {
            message = message + x + "\t";
            HashSet<String> rolesX = matrix.get(x);
            for(String role : this.roles) {
                if(rolesX.contains(role)) {
                    message = message + "+\t";
                } else {
                    message = message + "\t";
                }
            }
            message = message + "\n";
        }
        return message;
    }



}
