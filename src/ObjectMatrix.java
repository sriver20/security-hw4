import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.Scanner;
import java.util.HashMap;
import java.util.HashSet;

public class ObjectMatrix {

    Roles roles;
    HashMap<String, HashMap<String, HashSet<String>>> matrix;
    HashSet<String> resources;
    String fileName;

    public ObjectMatrix(String fileName, Roles roles, HashSet<String> resources ) {
        this.matrix = new HashMap<String, HashMap<String, HashSet<String>>>();
        this.roles = roles;
        this.resources = resources;
        this.fileName = fileName;
        System.out.println(nullString());
        setControlForSelf();
        setOwnerForDecendants();

        while(true) {
            try {
                ArrayList<String[]> fileData = new ArrayList<String[]>();
                System.out.println("Getting data");
                fileData = getFileData(this.fileName);
                System.out.println("Parsing data");
                parseFileData(fileData);
                break;
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            retry();
        }
        System.out.println(this);
    }

    public ArrayList<String []> getFileData(String FileName)
            throws IOException {
        ArrayList<String []> fileData = new ArrayList<String []>();
        Scanner scanner = new Scanner(new File(fileName));
        int lineCounter = 0;
        while(scanner.hasNextLine()) {
            lineCounter ++;
            String [] splitString = scanner.nextLine().toUpperCase().split("\\s+");
            int splitStringLength = splitString.length;
            if (splitStringLength != 3 ) {
                continue;
            }
            String [] tempArray = new String [splitStringLength + 1];
            int count = 0;
            for(String word : splitString) {
                tempArray[count] = new String(splitString[count]);
                count ++;
            }
            tempArray[splitStringLength] = Integer.toString(lineCounter);
            // for (String x : tempArray)
            //     System.out.print(" " + x);
            // System.out.println();
            fileData.add(tempArray);
        }
        scanner.close();
        return fileData;
    }

    public void parseFileData(ArrayList<String []> fileData)
            throws IOException {
        for(String[] line : fileData) {
            String role = line[0];
            String permission = line[1];
            String object = line[2];
            String lineNumber = line[3];
            System.out.println("Contains role: "
                    + this.roles.getRoles().contains(role));
            System.out.println("Object in role: "
                    + this.roles.getRoles().contains(object));
            System.out.println("Object in resouces: "
                    + this.resources.contains(object));
            if(this.roles.getRoles().contains(object) || this.resources.contains(object)
                    && this.roles.getRoles().contains(object)) {
                String message = "File: " + this.fileName + " Line: "
                    + lineNumber + " Invalid role or object.";
                throw new IOException(message);
            }
            setPermissionOfFamily(role, object, permission);
        }
    } //end of parseFileData()

    public void retry() {
        System.out.println("Fix the file then, press any key to continue.");
        Scanner s = new Scanner(System.in);
        String string = s.next();
        System.out.println("Trying again");
    }

    public void setControlForSelf() {
        for(String role : this.roles.getRoles()) {
            HashSet<String> permissions = new HashSet<String>();
            permissions.add(new String("c").toUpperCase());
            HashMap<String, HashSet<String>> objects = new
            HashMap<String, HashSet<String>>();
            objects.put(role, permissions);
            this.matrix.put(role, objects);

        }
    }

    public void addPermissionToRole(String role,String object,
            String permission) {
        HashMap<String, HashSet<String>> objects = this.matrix.get(role);
        if(objects.containsKey(object)) {
            HashSet<String> permissions = objects.get(object);
            if(!permissions.contains(permission)) {
                permissions.add(permission);
                objects.put(object, permissions);
                this.matrix.put(role, objects);
            }
        } else {
            HashSet<String> permissions = new HashSet<String>();
            permissions.add(permission);
            objects.put(object, permissions);
            this.matrix.put(role, objects);
        }
    }

    public void setPermissionOfFamily(String roleString, String object,
            String permission) {
        Role role = this.roles.getRole(roleString);
        addPermissionToRole(roleString, object, permission);
        String[] decendants = role.getAncestors();
        for(String decendant : decendants) {
            addPermissionToRole(decendant, object, permission);
        }
    }

    public void setOwnerForDecendants() {
        HashSet<String> roots = this.roles.getRoots();
        for(String stringRole : this.roles.getRoles()) {
            if (roots.contains(stringRole)) { continue; }
            Role role = this.roles.getRole(stringRole);
            String[] roleDecendants = role.getAncestors();
            for(String decendant : roleDecendants) {
                addPermissionToRole(decendant, stringRole, "O");
            }
        }
    }

    public HashMap<String, HashSet<String>> getRolesObjects(String role) {
        if(this.matrix.containsKey(role)) {
            return this.matrix.get(role);
        }
        return null;
    }

    public HashSet<String> getRolesObjectPermissions(String role, String object) {
        if(this.matrix.get(role).containsKey(object)) {
            return this.matrix.get(role).get(object);
        }
        return null;
    }

    public boolean isObject(String object) {
        if(this.matrix.containsKey(object) || this.resources.contains(object)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String message = "Roles and Objects\n";
        for(String role : this.roles.getRoles()) {
            message = message + "\t" + role;
        }
        for(String object : this.resources) {
            message = message + "\t" + object;
        }
        message = message + "\n";
        for(String row : this.roles.getRoles()) {
            message = message + row + "  ";
            HashMap<String,HashSet<String>> objects = matrix.get(row);
            for(String object : this.roles.getRoles()) {
                if(objects.containsKey(object)) {
                    HashSet<String> permissions = objects.get(object);
                    int count = 0;
                    for(String x : permissions) {
                        if(count < permissions.size() -1) {
                            message = message + x + ",";
                            count ++;
                        } else {
                            message = message + x + "\t";
                        }
                    }
                } else {
                    message = message + "n\t";
                }
            }
            for(String object : this.resources) {
                if(objects.containsKey(object)) {
                    HashSet<String> permissions = objects.get(object);
                    int count = 0;
                    for(String x : permissions) {
                        if(count < permissions.size() -1) {
                            message = message + x + ",";
                            count ++;
                        } else {
                            message = message + x + "\t";
                        }
                    }
                } else {
                    message = message + "n\t";
                }
            }
            message = message + "\n";
        }
        return message + "\n";
    }

    public String nullString() {
        String message = "Roles and Objects\n";
        for(String role : this.roles.getRoles()) {
            message = message + "\t" + role;
        }
        for(String object : this.resources) {
            message = message + "\t" + object;
        }
        message = message + "\n";
        for(String row : this.roles.getRoles()) {
            message = message + row + "  ";
            HashMap<String,HashSet<String>> objects = matrix.get(row);
            for(String object : this.roles.getRoles()) {
                message = message + "n\t";
            }
            for(String object : this.resources) {
                message = message + "n\t";
            }
            message = message + "\n";
        }
        return message + "\n";
    }
}
