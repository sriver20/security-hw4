
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.io.IOException;
import java.io.PrintWriter;

public class ResourcesTest {

    Resources resources1;
    Resources resources2;
    Resources resources3;

    ArrayList<String []> bad;
    ArrayList<String []> example;
    ArrayList<String []> test;

    String [] stringArray1 = {"F1", "F2", "F3", "P1", "P2", "P3", "D1", "D2"};
    String [] stringArray2 = {"F1", "F1"};
    String [] stringArray3 = {"F1", "F2", "F3", "F3"};

    @Before
    public void setUp() throws Exception {

        bad = new ArrayList<String[]>();
        example = new ArrayList<String[]>();
        test = new ArrayList<String[]>();

        bad.add(stringArray1);
        example.add(stringArray2);
        test.add(stringArray3);

    }

    @Test
    public final void testParseFileData() {
        System.out.println("Resources 1");
        try {
            resources1 = new Resources();
            resources1.parseFileData(bad);
            for (String x : resources1.getResources()) {
                System.out.println(x + " ");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\nResources 2");
        try {
            resources2 = new Resources();
            resources2.parseFileData(example);
            for (String x : resources2.getResources()) {
                System.out.println(x + " ");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\nResources 3");
        try {
            resources3 = new Resources();
            resources3.parseFileData(test);
            for (String x : resources3.getResources()) {
                System.out.println(x + " ");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
