import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.io.IOException;

public class RolesTest {
    Roles roles1;
    Roles roles2;
    Roles roles3;

    ArrayList<String []> badTree;
    ArrayList<String []> example;
    ArrayList<String []> test;

    String [] tree1r1 = {"R4", "R2", "1" };
    String [] tree1r2 = {"R4", "R3", "2" };
    String [] tree1r3 = {"R2", "R1", "3" };
    String [] tree1r4 = {"R3", "R1", "4" };

    String [] example1 = {"R8", "R6", "1" };
    String [] example2 = {"R4", "R2", "2" };
    String [] example3 = {"R5", "R2", "3" };
    String [] example4 = {"R6", "R2", "4" };
    String [] example5 = {"R2", "R1", "5" };
    String [] example6 = {"R9", "R7", "6" };
    String [] example7 = {"R10", "R7", "7" };
    String [] example8 = {"R7", "R3", "8" };
    String [] example9 = {"R3", "R1", "9" };

    String [] test1 = {"R2", "R1", "1" };
    String [] test2 = {"R3", "R1", "2" };
    String [] test3 = {"R4", "R1", "3" };
    String [] test4 = {"R5", "R1", "4" };
    String [] test5 = {"R6", "R1", "5" };
    String [] test6 = {"R7", "R1", "6" };

    @Before
    public void setUp() {
        roles1 = new Roles();
        roles2 = new Roles();
        roles3 = new Roles();

        badTree = new ArrayList<String []>();
        example = new ArrayList<String []>();
        test = new ArrayList<String []>();


        badTree.add(tree1r1);
        badTree.add(tree1r2);
        badTree.add(tree1r3);
        badTree.add(tree1r4);

        example.add(example1);
        example.add(example2);
        example.add(example3);
        example.add(example4);
        example.add(example5);
        example.add(example6);
        example.add(example7);
        example.add(example8);
        example.add(example9);

        test.add(test1);
        test.add(test2);
        test.add(test3);
        test.add(test4);
        test.add(test5);
        test.add(test6);
    }

    @Test
    public final void testParseFileData() {
        System.out.println("Role 1");
        try {
            roles1.parseFileData(example);
            roles1.setRoots();
            System.out.println(roles1);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\nRole 2");
        try {
            roles2.parseFileData(test);
            roles2.setRoots();
            System.out.println(roles2);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\nRole 3");
        try {
            roles3.parseFileData(badTree);
            roles3.setRoots();
            System.out.println(roles3);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
