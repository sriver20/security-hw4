import javax.swing.JOptionPane;
import java.io.IOException;
import java.util.HashSet;

public class Query {

    String userString, objectString;
    String [] permissions;

    public Query() {
        int count = 0;
        while (true) {
            try {
                if (count < 1) {
                    this.userString = setUser(true);
                } else {
                    this.userString = setUser(false);
                }
                break;
            } catch (IOException e) {
                count ++;
                System.out.println(e.getMessage());
            }
        }

        count = 0;
        while (true) {
            try {
                if (count < 1) {
                    count ++;
                this.objectString = setObject(true);
                } else {
                    count ++;
                this.objectString = setObject(false);
                }
                break;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        while (true) {
            try {
                this.permissions = setPermissions();
                break;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    public String setUser(boolean firstTime) throws IOException{
        String message = "Please enter the user in you query:";
        if(!firstTime) {
            message = "That was not a valid user.\n\n" + message;
        }
        String line = JOptionPane.showInputDialog(message);
        if(line == null) { line = ""; }
        String [] splitString = line.toUpperCase().split("\\s+");
        int wordCount = splitString.length;
        if (wordCount != 1) {
            System.out.println();
            String errorMessage = "That is not a valid user.";
            throw new IOException(errorMessage);
        }
        System.out.println(splitString[0]);
        System.out.println();
        return (new String(splitString[0]).toUpperCase());
    }

    public String setObject(boolean firstTime) throws IOException{
        String message = "Please enter the object in your query " +
                         "(hit enter if it's for any):";
        if(!firstTime) {
            message = "Only enter one object at a time.\n\n" + message;
        }
        String line = JOptionPane.showInputDialog(message);
        if(line == null) { line = ""; }
        String [] splitString = line.toUpperCase().split("\\s+");
        int wordCount = splitString.length;
        if (wordCount > 1) {
            System.out.println();
            String errorMessage = "That is not a valid object.";
            throw new IOException(errorMessage);
        }
        System.out.println();
        if (wordCount == 1 ) {
            System.out.println(splitString[0]);
            return (new String(splitString[0]).toUpperCase());
        } else {
            return "";
        }
    }


    public String[] setPermissions() throws IOException{
        String message = "Please enter the access right in your query " +
                         "(hit enter if it's for any):";
        String line = JOptionPane.showInputDialog(message);
        if(line == null) { line = ""; }
        if (line.equals("")) { line = " "; }
        String [] splitString = line.toUpperCase().split("\\s+");
        for(String x : splitString)
            System.out.print(x + " ");
        System.out.println();
        return splitString;
    }

    public String getUser() {
        return this.userString;
    }

    public String getObject() {
        return this.objectString;
    }

    public String[] getPermissions() {
        return this.permissions;
    }


}
