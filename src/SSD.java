import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Scanner;


public class SSD {

    HashMap<Integer, HashSet<String>> constraints;
    String fileName;


    public SSD (String fileName) {
        this.constraints = new HashMap<Integer, HashSet<String>>();
        this.fileName = fileName;

        while(true) {
            try {
                ArrayList<String[]> fileData = new ArrayList<String[]>();
                fileData = getFileData(this.fileName);
                parseFileData(fileData);
                break;
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            retry();
        }
        System.out.println(this);
    }

    public SSD () {
        this.constraints = new HashMap<Integer, HashSet<String>>();
        this.fileName = "roleSetsSSD.txt";
    }

    public void retry() {
        System.out.println("Fix the file then, press any key to continue.");
        Scanner s = new Scanner(System.in);
        String string = s.next();
        System.out.println("Trying again");
    }

    public ArrayList<String []> getFileData(String FileName)
            throws IOException {
        ArrayList<String []> fileData = new ArrayList<String []>();
        Scanner scanner = new Scanner(new File(fileName));
        int lineCounter = 0;
        while(scanner.hasNextLine()) {
            lineCounter ++;
            String [] splitString = scanner.nextLine().toUpperCase().split("\\s+");
            int splitStringLength = splitString.length;
            if (splitStringLength < 1 ) {
                continue;
            }
            String [] tempArray = new String [splitStringLength + 1];
            int count = 0;
            for(String word : splitString) {
                tempArray[count] = new String(splitString[count]);
                count ++;
            }
            tempArray[splitStringLength] = Integer.toString(lineCounter);
            // for (String x : tempArray)
            //     System.out.print(" " + x);
            // System.out.println();
            fileData.add(tempArray);
        }
        scanner.close();
        return fileData;
    }

    public void parseFileData(ArrayList<String []> fileData)
            throws IOException {
        for(String[] line : fileData) {
            HashSet<String> roleSet = new HashSet<String>();
            int n = Integer.parseInt(line[0]);
            int indexOfLineNumber = line.length - 1;
            String lineNumber = line[indexOfLineNumber];
            if(constraints.containsKey(n)) {
                String message = "Found duplicate n: " + n + " Line: "
                                 + lineNumber + " File Name: " + this.fileName;
                throw new IOException(message);
            }
            for(int i = 1; i < indexOfLineNumber; i++) {
                roleSet.add(line[i]);
            }
            constraints.put(n, roleSet);
        }
    } //end of parseFileData()

    public boolean doRolesSatisfySSD(String [] roles) {
        int roleSize = roles.length;
        while(true) {
            if(constraints.containsKey(roleSize)) {
                break;
            }
            roleSize --;
        }
        HashSet<String> roleSet = constraints.get(roleSize);
        boolean containsAll = true;
        for(String role : roles) {
            if(!roleSet.contains(role)) {
                containsAll = false;
                break;
            }
        }
        return containsAll;
    }

    @Override
    public String toString() {
        String message = "SSD\n";
        int counter  = 1;
        for(Integer x : constraints.keySet()) {
            message = message + "Constraint: " + counter
                      + " n = " + x + ", set of roles = {";
            HashSet<String> temp = constraints.get(x);
            int tempCount = 0;
            for(String role : temp) {
                if(tempCount < temp.size() - 1) {
                    message = message + role + ", ";
                } else {
                    message = message + role;
                }
                tempCount ++;
            }
            message = message + "}\n";
            counter ++;
        }
        return message;
    }

}
