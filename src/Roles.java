import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;



public class Roles {

    private HashMap<String, Role> roles;
    private String fileName;
    private HashSet<String> roots;

    public Roles(String fileName) {
        this.roles = new HashMap<String, Role>();
        this.fileName = fileName;
        this.roots = new HashSet<String>();

        ArrayList<String []> fileData;
        while(true) {
            try {
                fileData = getFileData(this.fileName);
                parseFileData(fileData);
                break;
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
            retry();
        }
        setRoots();
        for (String role : this.roles.keySet()) {
            getAncestors(role);
        }
        // for (String role : this.roles.keySet()) {
        //     System.out.println(this.roles.get(role));
        // }
        System.out.println(this);
    } // End of Roles()

    /**
     * This Constructor was made for testing only
     *
    **/
    public Roles() {
        this.fileName = "roleHierarchy.txt";
        this.roles = new HashMap<String, Role>();
        this.roots = new HashSet<String>();
    }

    public void retry() {
        System.out.println("Fix the file then, press any key to continue.");
        Scanner s = new Scanner(System.in);
        String string = s.next();
        System.out.println("Trying again");
    }

    public ArrayList<String []> getFileData(String fileName)
            throws IOException {
        ArrayList<String []> fileData = new ArrayList<String []>();
        Scanner scanner = new Scanner(new File(fileName));
        int lineCounter = 0;
        while(scanner.hasNextLine()) {
            lineCounter ++;
            String [] splitString = scanner.nextLine().toUpperCase().split("\\s+");
            int splitStringLength = splitString.length;
            if (splitStringLength > 2 ) {
                // String message = "File: " + fileName + " Line: " + lineCounter
                //                  + " contains to many words. "
                //                  + "Requires: 2 words.";
                // throw new IOException(message);
                continue;
            }
            if (splitStringLength < 2) {
                // String message = "File: " + fileName + " Line: " + lineCounter
                //                  + " contains one words. Requires: 2 words.";
                // throw new IOException(message);
                continue;
            }
            String [] tempArray = new String [3];
            tempArray[0] = new String(splitString[0]);
            tempArray[1] = new String(splitString[1]);
            tempArray[2] = Integer.toString(lineCounter);
            // for (String x : tempArray)
            //     System.out.print(" " + x);
            // System.out.println();
            fileData.add(tempArray);
        }
        scanner.close();
        return fileData;
    } // End of getDataFromFile()


    public void parseFileData(ArrayList<String []> fileData)
            throws IOException {
        for(String[] line : fileData) {
            String child = new String(line[0]).toUpperCase();
            String parent = new String(line[1]).toUpperCase();
            // System.out.println("Checking child: " + child + " parent: "
            //                    +  parent);
            boolean rolesContainsChild = this.roles.containsKey(child);
            boolean rolesContainsParent = this.roles.containsKey(parent);
            if (!rolesContainsChild  && !rolesContainsParent) {
                addChildToRoles(child, parent);
                addParentToRoles(child, parent);
            } else if (!rolesContainsChild && rolesContainsParent) {
                updateParentInRoles(child, parent, line[2]);
                addChildToRoles(child, parent);
            } else if (rolesContainsChild && !rolesContainsParent) {
                updateChildInRoles(child, parent, line[2]);
                addParentToRoles(child, parent);
            } else {
                Role childRole = this.roles.get(child);
                Role parentRole = this.roles.get(parent);
                if (childRole.getParent() == null) {
                    childRole.setParent(parent);
                    parentRole.addChild(child);
                    // System.out.println("Updating parent: " + parent);
                    // System.out.println("Updating child: " + child);
                    this.roles.put(child, childRole);
                    this.roles.put(parent, parentRole);
                    // System.out.println("Done checking child: " + child
                    //                    + " parent: "
                    //                    +  parent + "\n");
                    continue;
                }
                boolean correctParent = childRole.isParent(parent);
                boolean correctChild = parentRole.isChild(child);
                if (!correctParent || !correctChild) {
                    String message = "Line: " + line[2] + " File Name: "
                                     + this.fileName
                                     + " contains invalid values.";
                    throw new IOException(message);
                }
                String message = "Line: " + line[2]
                                 + " contains duplicate data.";
                System.out.println(message);
            }
            // System.out.println("Done checking child: " + child + " parent: "
            //                    +  parent + "\n");
        }
    } //end of parseFileData()

    public void addChildToRoles(String child,String parent) {
        // System.out.println("Add child: " + child + " parent: " + parent);
        Role childRole = new Role(child);
        childRole.setParent(parent);
        this.roles.put(child, childRole);
    }

    public void addParentToRoles(String child, String parent) {
        // System.out.println("Add parent: " + parent + " child: " + child );
        Role parentRole = new Role(parent);
        parentRole.addChild(child);
        this.roles.put(parent, parentRole);
    }

    public void updateChildInRoles(String child, String parent, String line)
            throws IOException {
        Role childRole = this.roles.get(child);
        String childsParent = childRole.getParent();
        if (childsParent == null) {
            childRole.setParent(parent);
            this.roles.put(child, childRole);
        } else if (childRole.isParent(parent)) {
            String message = "Line: " + line + " contains duplicate data.";
            System.out.println(message);
        } else {
            String message = "Line: " + line + " contains an invalid Parent."
                             + "\nGiven Parent: " + parent
                             + " Expected Parent: " + childRole.getParent();
            throw new IOException(message);
        }
    }

    public void updateParentInRoles(String child, String parent, String line)
            throws IOException {
        Role parentRole = this.roles.get(parent);
        if (parentRole.isChild(child)) {
            String message = "Line: " + line + ", contains duplicate data.";
            System.out.println(message);
        } else if(parentRole.isParent(child)) {
            String message = "Line: " + line + " contains an invalid child."
                             + "\nGiven child: " + child + " is also parent.";
            throw new IOException(message);
        } else
            parentRole.addChild(child);
            this.roles.put(parent, parentRole);
    }

    public void setRoots() {
        for (Role role : this.roles.values()) {
            if (role.getParent() == null) {
                this.roots.add(role.getName());
            }
        }
        String message = "Roots are: ";
        int rootCount = 0;
        for(String x : this.roots) {
            if (rootCount < this.roots.size() - 1) {
                message = message + x + ", ";
                rootCount ++;
            } else {
                message = message + x;
            }
        }
        System.out.println(message + "\n");
    }

    public HashSet<String> getRoots() {
        return this.roots;

    }

    public HashSet<String> getRoles() {
        HashSet<String> temp = new HashSet<String>();
        for(String x : this.roles.keySet()) {
            temp.add(x);
        }
        return temp;
    }

    public Role getRole(String role) {
        if(this.roles.containsKey(role)) {
            return this.roles.get(role);
        }
        return null;
    }

    @Override
    public String toString() {
        String message = "Role Hierarchy";
        Queue<String> queue = new LinkedList<String>();
        for(String root : roots) {
            queue.add(root);
            while (queue.size() > 0) {
                String visted = queue.remove();
                message = message + "\n" + visted + " ---> ";
                Role role = this.roles.get(visted);
                if(role.hasChildren()) {
                    for(String child : role.getChildren()) {
                        message = message + child + " ";
                        queue.add(child);
                    }
                }

            }
        }
        return  message + "\n";
    }

    public void getAncestors(String starterRole) {
        Role mainRole = this.roles.get(starterRole);
        Queue<String> queue = new LinkedList<String>();
        queue.add(starterRole);
        String visted = starterRole;
        int count = 0;
        while (queue.size() > 0) {
            visted = queue.remove();
            Role role = this.roles.get(visted);
            String parent = role.getParent();
            if(parent != null) {
                if(count > 0 ) {
                    mainRole.addAncestors(visted);
                }
                count ++;
                queue.add(parent);
            }
        }
        mainRole.addAncestors(visted);
    }


}

