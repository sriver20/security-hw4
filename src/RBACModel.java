import javax.swing.JOptionPane;
import java.util.HashSet;
import java.util.HashMap;

public class RBACModel {

    String rolesFileName = "roleHierarchy.txt";
    String resourcesFileName = "resourceObjects.txt";
    String permissionsFileName = "permissionsToRoles.txt";
    String ssdFileName = "roleSetsSSD.txt";
    String userRoleFileName = "usersRoles.txt";
    Roles roles;
    Resources resources;
    ObjectMatrix objectMatrix;
    SSD ssd;
    UserMatrix userMatrix;
    HashSet<String> rolesSet, resourcesSet;
    Query query;


    public RBACModel () {
        this.roles = new Roles(this.rolesFileName);
        this.rolesSet = roles.getRoles();
        this.resources = new Resources(this.resourcesFileName);
        this.resourcesSet = resources.getResources();
        this.objectMatrix = new ObjectMatrix(this.permissionsFileName,
                this.roles, this.resourcesSet);
        this.ssd = new SSD(this.ssdFileName);
        this.userMatrix = new UserMatrix(this.userRoleFileName,
                                         this.ssd, this.rolesSet);
        boolean retry = true;
        while(retry) {
            this.query = new Query();
            String user = this.query.getUser();
            String object = this.query.getObject();
            String[] permissions = this.query.getPermissions();

            if(checkInputs(user, object)) {
                int objectLength = object.length();
                int permissionLength = permissions.length;

                if(objectLength == 0 && permissionLength == 0) {
                    String m = getPermissionUser(user);
                    System.out.println(m);
                } else if(objectLength > 0 && permissionLength == 0) {
                    String m = getPermissionUserObject(user, object);
                    System.out.println(m);
                } else if(objectLength == 0 && permissionLength > 0) {
                    String m = getPermissionUserAccess(user, permissions);
                    System.out.println(m);
                } else {
                    String m = getPermissionUserObjectAccess(user ,object
                                                            ,permissions);
                    System.out.println(m);
                }
            }
            retry = retry();
        }

    }

    public RBACModel(String user, String object, String[] permissions) {
        this.roles = new Roles(this.rolesFileName);
        this.rolesSet = roles.getRoles();
        this.resources = new Resources(this.resourcesFileName);
        this.resourcesSet = resources.getResources();
        this.objectMatrix = new ObjectMatrix(this.permissionsFileName,
                this.roles, this.resourcesSet);
        this.ssd = new SSD(this.ssdFileName);
        this.userMatrix = new UserMatrix(this.userRoleFileName,
                                         this.ssd, this.rolesSet);

        if(checkInputs(user, object)) {
            int objectLength = object.length();
            int permissionLength = permissions.length;
            if(objectLength == 0 && permissionLength == 0) {
                String m = getPermissionUser(user);
                System.out.println(m);
            } else if(objectLength > 0 && permissionLength == 0) {
                String m = getPermissionUserObject(user, object);
                System.out.println(m);
            } else if(objectLength == 0 && permissionLength > 0) {
                String m = getPermissionUserAccess(user, permissions);
                System.out.println(m);
            } else {
                String m = getPermissionUserObjectAccess(user ,object
                                                        ,permissions);
                System.out.println(m);
            }
        }
    }

    public boolean retry() {
        HashSet<String> yesResponces = new HashSet<String>();
        yesResponces.add("Y");
        yesResponces.add("YE");
        yesResponces.add("YES");

        String message = "Would you like to restart?";
        String line = JOptionPane.showInputDialog(message);
        System.out.println(line);
        if(line == null) { return false; }
        String [] splitString = line.toUpperCase().split("\\s+");
        int wordCount = splitString.length;
        if( wordCount < 1) { return false; }
        if(yesResponces.contains(splitString[0].toUpperCase()) &&
                wordCount == 1) {
            return true;
        }
        return false;
    }

    public boolean checkInputs(String user, String object) {
        int objectLength = object.length();
        if (!this.userMatrix.isUser(user)) {
            System.out.println("Invalid User");
            return false;
        }
        if(objectLength > 0) {
            if(this.objectMatrix.isObject(object)) {
                return true;
            }
            System.out.println("Invalid Object");
            return false;
        }
        return true;
    }

    public String getPermissionUser(String user) {
        HashMap<String, HashSet<String>> results =
            new HashMap<String, HashSet<String>>();
        HashSet<String> userRoles = this.userMatrix.getRoles(user);
        for(String role : userRoles) {
            HashMap<String, HashSet<String>> objects =
                this.objectMatrix.getRolesObjects(role);
            for(String object : objects.keySet()) {
                HashSet<String> permissions = objects.get(object);
                for(String permission : permissions){
                    if(results.containsKey(object)) {
                        HashSet<String> resultPermission =
                            results.get(object);
                        resultPermission.add(permission);
                        results.put(object, resultPermission);
                    } else {
                        HashSet<String> resultPermission =
                            new HashSet<String>();
                        resultPermission.add(permission);
                        results.put(object, resultPermission);
                    }
                }
            }
        }
        String message = "Invalid user query. Try again";
        if(results.size() != 0) {
            message = "Users: "+ user +" has access to:\n";
            for(String object : results.keySet()) {
                message = message + object + "\t";
                for( String perm : results.get(object)) {
                    message = message + perm + " ";
                }
            message = message + "\n";
            }
        }
        return message + "\n";
    }

    public String getPermissionUserObject(String user, String object) {
        HashSet<String> results = new HashSet<String>();
        HashSet<String> userRoles = userMatrix.getRoles(user);
        for(String role : userRoles) {
            HashSet<String> rolePermissions =
                this.objectMatrix.getRolesObjectPermissions(role, object);
            if(rolePermissions == null) {
                continue;
            }
            for(String permission : rolePermissions) {
                results.add(permission);
            }
        }
        String message = "Invalid user and object query. Try again";
        if(results.size() != 0) {
            message = "User: " + user + " has access to:\n" + object +"\t";
            for (String x : results) {
                message = message + x + " ";
            }
            message = message + "\n";
        }
        return message;
    }

    public String getPermissionUserAccess(String user, String[] access) {
        HashSet<String> results = new HashSet<String>();
        HashSet<String> userRoles = userMatrix.getRoles(user);
        for(String role : userRoles) {
            HashMap<String, HashSet<String>> objects =
                this.objectMatrix.getRolesObjects(role);
            for(String object : objects.keySet()) {
                HashSet<String> permission = objects.get(object);
                if(permission.contains(access)){
                    results.add(object);
                }

            }
        }
        String accessString = null;
        int count = 0 ;
        for(String x : access) {
            if ( count < access.length - 1) {
                accessString = accessString + access + " or ";
            } else {
                accessString = accessString + access;
            }
        }

        String message = "Invalid user, access query. Try again";
        if(results.size() != 0) {
            message = "User: " + user + " has access to:\n";
            for (String x : results) {
                message = message + x  + "\t" + accessString;
            }
            message = message + "\n";
        }
        return message;
    }

    public String getPermissionUserObjectAccess(String user, String object,
            String[] access) {
        HashSet<String> results = new HashSet<String>();
        HashSet<String> userRoles = userMatrix.getRoles(user);
        for(String role : userRoles) {
            HashSet<String> temp = this.objectMatrix.
                getRolesObjectPermissions(role, object);
            if(temp != null && temp.contains(access)) {
                results.add(object);
            }
        }
        String accessString = null;
        int count = 0 ;
        for(String x : access) {
            if ( count < access.length - 1) {
                accessString = accessString + access + " or ";
            } else {
                accessString = accessString + access;
            }
        }

        String message = "Invalid user, object, access right serach." +
                         " Try again";
        if(results.size() != 0) {
            message = "User: " + user + " has access to:\n";
            for (String x : results) {
                message = message + x + "\t" + accessString;
            }
            message = message + "\n";
        }
        return message;
    }

    public static void main(String [] args) {
        RBACModel model = new RBACModel();
    }


}
