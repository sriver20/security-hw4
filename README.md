Simplified NIST RBAC Model
==========================

Author: Salvador Rivera

Version: CS3750

Website: https://sriver20@bitbucket.org/sriver20/simplified-nist-rbac-model.git

Manually Compile and Run
------------------------
1. Go into the src directory
2. Compile the java source code
3. Copy the text files into the source directory.
4. Run the java source code - RBACModel contains the main function.

```sh
cd src/
javac *.java
cp resources/*.txt .
java RBACModel
```


Ant
---
Default target: run

 clean    Removes doc, test, dist directories and empties the bin directory
 compile  Compiles java files except test files.
 doc      Generates API documentation.
 jar      Creates a jar file.
 report   Generates a report of the Junit tests.
 run      Runs the jar, same as java -jar
 test     Runs all Junit tests.

Requirements
------------
* java, javac, javadoc, jar (code compatible with Java Platform SE 8*)
* ant (script compatible with Apache Ant 1.9.3)
* junit (code compatible with JUnit 4.6.2) included in source distribution.

*Although Java 8 has several desirable new features, Java 7 may be a safer
 choice to enhance the likelihood of compatibility with test systems.
